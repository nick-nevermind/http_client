#ifndef http_client_header
#define http_client_header

#include <linux/limits.h>

enum
{
	DEFAULT_HTTP_PORT = 80,
	DEFAULT_BUFFER_SIZE = 32768
};

static const char DEFAULT_OUTPUT_FILE[] = "out.html";

typedef struct _url_parts
{
	char *scheme;
	char *host;
	char *port;
	char *path_and_query;
	char *fragment;
} url_parts;

typedef struct _app_options
{
	char output_file[PATH_MAX];
	int output_stdout;
	char input_url[ARG_MAX];
	int buffer_size;
} app_options;

typedef struct _app_context
{
	app_options options;
	url_parts *url;
	int sock;
	FILE *output_file;

	struct
	{
		char *ptr;
		size_t used;
	} buffer;
} app_context;

app_context * app_context_init(int argc, char **argv);
void app_context_free(app_context *context);
void app_exit(app_context *context, char *exit_msg, int error_code);
void app_options_parse(int argc, char **argv, app_context *context);
void app_usage_print(const char *exec_name);
struct in_addr app_resolve_host(app_context *ctx);
void app_connect(app_context *ctx, struct in_addr ip);
void app_send_request(app_context *ctx);

typedef const char * (*read_condition_t)(const char *buffer, int size);

char * buffer_condition_read(app_context *ctx, read_condition_t condition, int *block_size, int max_size);

const char * condition_new_line(const char *buffer, int size);
const char * condition_response_header(const char *buffer, int size);
const char * condition_full_buffer(const char *buffer, int size);

int socket_write_full(int fd, void *buffer, int len);
int socket_read_full(int fd, void *buffer, int len);

url_parts * url_parse(char *input);
void url_parts_free(url_parts *parts);

#endif
