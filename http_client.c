#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <ctype.h>

#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>

#include "http_client.h"

int main(int argc, char **argv)
{
	app_context *ctx = app_context_init(argc, argv);

	ctx->url = url_parse(ctx->options.input_url);

	if (NULL == ctx->url)
		app_exit(ctx, "URL parse error", EXIT_FAILURE);

	if (0 != strcmp(ctx->url->scheme, "http"))
		app_exit(ctx, "Only http scheme is supported.", EXIT_FAILURE);

	struct in_addr ip = app_resolve_host(ctx);
	app_connect(ctx, ip);
	app_send_request(ctx);

	fprintf(stderr, "Receiving HTTP response ... ");
	int readed = 0;
	char *header = buffer_condition_read(ctx, condition_response_header, &readed, 0);
	int header_size = readed;

	if (NULL == header)
		app_exit(ctx, "Failed.", EXIT_FAILURE);

	ctx->output_file = ctx->options.output_stdout
						? stdout
						: fopen(ctx->options.output_file, "wb");

	char *chunk = NULL, *chunk_header = NULL;
	int chunk_size = 0, sum_size = 0;

	// If chunked encoding.
	char *tmp = strstr(header, "Transfer-Encoding: chunked");
	if (NULL != tmp && tmp < header + header_size)
	{
		while (1)
		{
			chunk_header = buffer_condition_read(ctx, condition_new_line, &readed, 0);

			if (NULL != chunk_header)
			{
				sscanf(chunk_header, "%x", &chunk_size);
				free(chunk_header);

				if (chunk_size == 0)
					break;
			}
			else break;

			while (chunk_size > 0)
			{
				chunk = buffer_condition_read(ctx, condition_full_buffer, &readed, chunk_size);

				if (readed)
				{
					chunk_size -= readed;
					fwrite(chunk, readed, 1, ctx->output_file);
					sum_size += readed;
					free(chunk);
				}
				else break;
			}

			buffer_condition_read(ctx, condition_new_line, NULL, 0);
		}
	}
	// Simple reader.
	else
	{
		while (1)
		{
			chunk = buffer_condition_read(ctx, condition_full_buffer, &readed, 0);

			if (readed)
			{
				fwrite(chunk, readed, 1, ctx->output_file);
				sum_size += readed;
				free(chunk);
			}
			else break;
		}
	}

	fprintf(stderr, "OK.\n");

	free(header);
	app_context_free(ctx);

	return EXIT_SUCCESS;
}

app_context * app_context_init(int argc, char **argv)
{
	app_context *ctx = malloc(sizeof(app_context));
	memset(ctx, 0x00, sizeof(app_context));

	/* Default options. */
	ctx->options.buffer_size = DEFAULT_BUFFER_SIZE;
	strncpy(ctx->options.output_file, DEFAULT_OUTPUT_FILE,
			sizeof(ctx->options.output_file) / sizeof(ctx->options.output_file[0]));

	app_options_parse(argc, argv, ctx);

	ctx->buffer.ptr = malloc(ctx->options.buffer_size);
	ctx->sock = -1;

	return ctx;
}

void app_context_free(app_context *context)
{
	if (NULL != context)
	{
		if (NULL != context->buffer.ptr)
			free(context->buffer.ptr);

		if (NULL != context->output_file)
			fclose(context->output_file);

		if (-1 != context->sock)
			close(context->sock);

		url_parts_free(context->url);

		free(context);
	}
}

void app_exit(app_context *context, char *exit_msg, int error_code)
{
	app_context_free(context);
	fprintf(stderr, "%s", exit_msg);
	exit(error_code);
}

struct in_addr app_resolve_host(app_context *ctx)
{
	int i = 0;

	fprintf(stderr, "\nResolving `%s`... ", ctx->url->host);
	struct hostent *host = gethostbyname2(ctx->url->host, AF_INET);
	if (NULL == host)
		app_exit(ctx, "Failed.", EXIT_FAILURE);

	struct in_addr **addr_list = (struct in_addr **)host->h_addr_list;

	for (i = 0; NULL != addr_list[i]; ++i)
		fprintf(stderr, "%s ", inet_ntoa(*addr_list[i]));
	fprintf(stderr, "\n");

	return *addr_list[0];
}

void app_connect(app_context *ctx, struct in_addr ip)
{
	ctx->sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

	struct sockaddr_in san = { 0 };
	san.sin_family = AF_INET;
	san.sin_port = htons(NULL != ctx->url->port ? atoi(ctx->url->port) : DEFAULT_HTTP_PORT);
	san.sin_addr = ip;

	fprintf(stderr, "Connecting to `%s`... ", inet_ntoa(san.sin_addr));

	if (connect(ctx->sock, (const struct sockaddr *)&san, sizeof(san)) != 0)
		perror("connect"), app_exit(ctx, "", EXIT_FAILURE);

	fprintf(stderr, "OK.\n");
}

void app_send_request(app_context *ctx)
{
	int request_size = 0;

	fprintf(stderr, "Sending HTTP request ... ");
	request_size = sprintf(ctx->buffer.ptr, "GET %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n",
			ctx->url->path_and_query, ctx->url->host);

	int sended = socket_write_full(ctx->sock, ctx->buffer.ptr, request_size);
	if (!sended)
		app_exit(ctx, "Failed.", EXIT_FAILURE);
	fprintf(stderr, "OK.\n");
}

void app_options_parse(int argc, char **argv, app_context *context)
{
	const struct option options_def[] =
	{
		{ "help", no_argument, NULL, 'h' },
		{ "stdout", no_argument, NULL, 's' },
		{ "buffer_size", required_argument, NULL, 'b' },
		{ "output", required_argument, NULL, 'o' },
		{ 0, 0, 0, 0 }
	};

	char command = 0;

	while (command != -1)
	{
		switch (command = getopt_long(argc, argv, "hsb:o:", options_def, NULL))
		{
		case 'h':
			app_usage_print(argv[0]);
			app_exit(context, "", EXIT_SUCCESS);
		break;
		case 's':
			context->options.output_stdout = 1;
		break;
		case 'b':
			context->options.buffer_size = atol(optarg);
		break;
		case 'o':
			strncpy(context->options.output_file, optarg,
					sizeof(context->options.output_file) / sizeof(context->options.output_file[0]));
		break;
		}
	}

	if (optind < argc)
	{
		strncpy(context->options.input_url, argv[optind],
				sizeof(context->options.input_url) / sizeof(context->options.input_url[0]));
	}
	else
	{
		app_usage_print(argv[0]);
		app_exit(context, "", EXIT_SUCCESS);
	}
}

void app_usage_print(const char *exec_name)
{
	printf("Usage: %s [--stdout] [--buffer_size <size>] [--output <output>] <input>\n",
			exec_name);
}

int socket_write_full(int fd, void *buffer, int len)
{
	int res = -1, cur = 0;

	do
		res = write(fd, &((char *)buffer)[cur], len - cur);
	while ((res > 0 && (cur += res) < len) || (res == -1 && errno == EINTR));

	return res < 0 ? res : cur;
}

int socket_read_full(int fd, void *buffer, int len)
{
	int res = -1, cur = 0;

	do
		res = read(fd, &((char *)buffer)[cur], len - cur);
	while ((res > 0 && (cur += res) < len) || (res == -1 && errno == EINTR));

	return res < 0 ? res : cur;
}

const char * condition_new_line(const char *buffer, int size)
{
	char *end_of_line = memchr(buffer, '\n', size);

	if (NULL != end_of_line)
		return end_of_line + 1;
	else
		return NULL;
}

char * buffer_condition_read(app_context *ctx, read_condition_t condition, int *out_block_size, int max_size)
{
	char *block = NULL;
	int block_size = 0;
	int readed = socket_read_full(ctx->sock, &ctx->buffer.ptr[ctx->buffer.used], ctx->options.buffer_size - ctx->buffer.used);

	if (readed < 0)
	{
		if (NULL != out_block_size)
			*out_block_size = 0;

		return NULL;
	}

	ctx->buffer.used += readed;

	char *end = (char *)condition(ctx->buffer.ptr, ctx->buffer.used);

	if (NULL == end)
		end = &ctx->buffer.ptr[ctx->buffer.used];

	if (max_size && end - ctx->buffer.ptr > max_size)
		end = &ctx->buffer.ptr[max_size];

	block_size = end - ctx->buffer.ptr;

	if (block_size != 0)
	{
		block = malloc(sizeof(char) * block_size);
		memcpy(block, ctx->buffer.ptr, block_size);
		ctx->buffer.used -= block_size;
	}

	if (NULL != out_block_size)
		*out_block_size = block_size;

	memmove(ctx->buffer.ptr, end, ctx->buffer.used);

	return block;
}

const char * condition_response_header(const char *buffer, int size)
{
	const char *cur = buffer;

	// Search for \n\r\n or \n\n.
	for (; cur < &buffer[size - 2]; ++cur)
	{
		if (cur[0] == '\n' && cur[1] == '\r' && cur[2] == '\n')
			return cur + 3;
		else if (cur[0] == '\n' && cur[2] == '\n')
			return cur + 2;
	}

	return NULL;
}

const char * condition_full_buffer(const char *buffer, int size)
{
	return NULL;
}

url_parts * url_parse(char *input)
{
	url_parts *output = (url_parts *)malloc(sizeof(url_parts));
	const char *begin = input, *end = NULL;
	size_t i = 0, len = 0;

	memset(output, 0x00, sizeof(url_parts));

	end = strchr(begin, ':');

	if (NULL == end)
	{
		url_parts_free(output);
		return NULL;
	}

	len = end - begin;
	output->scheme = malloc(len + 1);

	for (i = 0; i < len; ++i)
		output->scheme[i] = tolower((int)begin[i]);

	output->scheme[len] = '\0';

	begin = ++end;

	for (end = begin + 2; begin < end; ++begin)
	{
		if ('/' != *begin)
		{
			url_parts_free(output);
			return NULL;
		}
	}

	while ('\0' != *end && ':' != *end && '/' != *end)
		++end;

	len = end - begin;

	output->host = malloc(len + 1);
	strncpy(output->host, begin, len);
	output->host[len] = '\0';

	begin = end;

	if (':' == *begin)
	{
		end = ++begin;

		while ('\0' != *end && '/' != *end)
			++end;

		len = end - begin;
		output->port = malloc(len + 1);
		strncpy(output->port, begin, len);
		output->port[len] = '\0';

		begin = end;
	}

	while ('\0' != *end && '#' != *end)
		++end;

	len = end - begin;
	output->path_and_query = malloc(len + 1);
	strncpy(output->path_and_query, begin, len);
	output->path_and_query[len] = '\0';

	begin = end;

	if ('#' == *begin)
	{
		end = ++begin;

		while ('\0' != *end)
			++end;

		len = end - begin;
		output->fragment = malloc(len);
		strncpy(output->fragment, begin, len);
		output->fragment[len] = '\0';

		begin = end;
	}

	return output;
}

void url_parts_free(url_parts *parts)
{
	if (NULL != parts)
	{
		if (NULL != parts->scheme)
			free(parts->scheme);

		if (NULL != parts->host)
			free(parts->host);

		if (NULL != parts->port)
			free(parts->port);

		if (NULL != parts->path_and_query)
			free(parts->path_and_query);

		if (NULL != parts->fragment)
			free(parts->fragment);

		free(parts);
	}
}
